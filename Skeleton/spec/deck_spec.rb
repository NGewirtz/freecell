require 'deck'

describe Deck do
  subject(:deck) { Deck.new }

  describe ':get_cards' do
    it 'creates 52 cards' do
      expect(Deck.get_cards.length).to eq(52)
    end
  end

  describe '#initialize' do
    it 'initializes with a shuffled deck' do
      expect(Deck.get_cards).not_to eq(deck.cards)
    end
  end

  describe '#deal' do
    it 'returns the correct number of cards' do
      expect(deck.deal(5).count).to eq(5)
    end

    it 'raises an error if there arent enough cards in the deck' do
      expect { deck.deal(53) }.to raise_error("Not enough cards!")
    end

    it 'removes cards that were taken from the deck' do
      deck.deal(12)
      expect(deck.cards.length).to eq(40)
    end
  end

end
