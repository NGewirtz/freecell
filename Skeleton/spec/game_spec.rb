require 'game'

describe Game do
  subject(:game) { Game.new }
  let(:three) { double("three", value: :three, suit: :spades) }
  let(:four) { double("three", value: :four, suit: :spades) }
  let(:ace) { double("ace", value: :ace, suit: :spades) }


  describe '#initialize' do
    it 'initializes with a new board and a player' do
      expect(game.board).to be_a(Board)
      expect(game.player).to be_a(Player)
    end
  end

  describe '#selected_card' do
    it "raises error if player selects pile incorrectly" do
      expect { game.selected_card("some invalid place", 5) }.to raise_error("Invalid pile!")
    end

    it "raises error if player selects index with no card present" do
      expect { game.selected_card(:free_cells, 2) }.to raise_error('No card here!')
      expect { game.selected_card(:tableau, -2) }.to raise_error('No card here!')
      expect { game.selected_card(:tableau, 15) }.to raise_error('No card here!')
    end

    it "selects a card with valid input" do
      expect(game.selected_card(:tableau, 3)).to be_a(Card)
      game.board.free_cells = [ [], [], [], [three]]
      expect(game.selected_card(:free_cells, 3)).to eq(three)
    end

  end

  describe "#move_to_destination" do
    before(:each) do
      game.move_to_destination(three, :free_cells, 2)
      game.move_to_destination(ace, :foundation_pile, 1)
    end

    it 'raises an error if player tries to make an illegal move' do
      expect { game.move_to_destination(four, :free_cells, 2) }.to raise_error("Cant move there")
      expect { game.move_to_destination(three, :foundation_pile, 2) }.to raise_error("Cant move there")
    end

    it 'makes legal moves' do
      expect(game.board.free_cells[2]).to include(three)
      expect(game.board.foundation_piles[1]).to include(ace)
    end
  end

  let(:player) { double("player", move_from: [:tableau, 1], move_to: [:free_cells, 0] ) }
  describe '#play' do
    it 'takes player input and makes valid moves' do
      expect(player).to receive(:move_from)
      expect(player).to receive(:move_to)
      expect(game.board.tableau[1].length).to eq(7)
      game.play_turn(player)
      expect(game.board.free_cells[0].last).to be_a(Card)
      expect(game.board.tableau[1].length).to eq(6)
    end
  end

  describe '#won' do
    it 'returns false if the game is not won' do
      expect(game.won?).to be false
    end

    it 'returns true when the game is won' do
      winning_board = Array.new(4) { Array.new(13) { "ace" } }
      game.board.foundation_piles = winning_board
      expect(game.won?).to be true
    end
  end
end
