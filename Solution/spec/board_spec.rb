require 'board'

describe Board do
  subject(:board) { Board.new }
  describe '#initialize' do

    it 'creates 4 free cells' do
      expect(board.free_cells.length).to eq(4)
      expect(board.free_cells.all?(&:empty?)).to be true
    end

    it 'creates 4 foundation piles' do
      expect(board.foundation_piles.length).to eq(4)
      expect(board.foundation_piles.all?(&:empty?)).to be true
    end

    context 'tableau piles' do
      it 'creates 8 tableau piles' do
        expect(board.tableau.length).to eq(8)
      end

      it 'fills each pile evenly with cards' do
        piles = board.tableau
        cards = piles.flatten
        expect(piles.count{ |pile| pile.length == 7}).to eq(4)
        expect(piles.count{ |pile| pile.length == 6}).to eq(4)
        expect(cards.all?{ |card| card.is_a?(Card) }).to be true
      end
    end
  end

  describe 'move validation' do

    let(:card) { "card" }
    let(:ace_c) { double('ace', value: :ace, suit: :clubs) }
    let(:ace_h) { double('ace', value: :ace, suit: :hearts) }
    let(:five) { double('five', value: :five, suit: :clubs) }
    let(:six_c) { double('six_c', value: :six, suit: :clubs) }
    let(:six_h) { double('six_h', value: :six, suit: :hearts) }
    let(:jack) { double('jack', value: :jack, suit: :clubs) }
    let(:queen) { double('jack', value: :queen, suit: :hearts) }

    context '#valid_free_cell_move?' do
      before(:each) do
        board.free_cells = [[], [], [card], []]
      end

      it 'checks if the cell is empty' do
        expect(board.valid_free_cell_move?(2)).to be false
        expect(board.valid_free_cell_move?(3)).to be true
      end

      it 'raises error if index is out of range' do
        expect{ board.valid_free_cell_move?(-2) }.to raise_error("Invalid index!")
        expect{ board.valid_free_cell_move?(6) }.to raise_error("Invalid index!")
      end
    end

    context '#valid_foundation_pile_move?' do

      before(:each) do
        board.foundation_piles = [[], [ace_c], [five], []]
      end

      it 'returns true if the pile is empty and you add any ace' do
        expect(board.valid_foundation_pile_move?(ace_c, 0)).to be true
        expect(board.valid_foundation_pile_move?(ace_h, 0)).to be true
      end

      it 'returns false if you try to add an ace to an occupied pile' do
        expect(board.valid_foundation_pile_move?(ace_c, 2)).to be false
        expect(board.valid_foundation_pile_move?(ace_h, 1)).to be false
      end

      it 'only allows you to add to pile with cards of the same suit' do
        expect(board.valid_foundation_pile_move?(six_c, 2)).to be true
        expect(board.valid_foundation_pile_move?(six_h, 2)).to be false
      end

      it 'only allows you to add to pile with cards of value 1 higher than top card' do
        expect(board.valid_foundation_pile_move?(jack, 2)).to be false
        expect(board.valid_foundation_pile_move?(ace_c, 2)).to be false
      end

      it 'raises error if index is out of range' do
        expect{ board.valid_foundation_pile_move?(jack, -2) }.to raise_error("Invalid index!")
        expect{ board.valid_foundation_pile_move?(jack, 6) }.to raise_error("Invalid index!")
      end
    end

    context '#valid_tableau_move?' do

      before(:each) do
        board.tableau = [[], [jack], [five], [six_c], [six_h], [], [], [queen]]
      end

      it 'allows you to move onto any empty pile' do
        expect(board.valid_tableau_move?(jack, 0)).to be true
        expect(board.valid_tableau_move?(jack, 5)).to be true
        expect(board.valid_tableau_move?(ace_c, 5)).to be true
      end

      it 'does not allows moves to piles with top cards of the same color suit' do
        expect(board.valid_tableau_move?(five, 3)).to be false
        expect(board.valid_tableau_move?(five, 4)).to be true
      end

      it 'only allows moves onto piles with top card value 1 higher than card' do
        expect(board.valid_tableau_move?(jack, 7)).to be true
        expect(board.valid_tableau_move?(queen, 1)).to be false
      end

      it 'raises error if index is out of range' do
        expect{ board.valid_tableau_move?(jack, -2) }.to raise_error("Invalid index!")
        expect{ board.valid_tableau_move?(jack, 10) }.to raise_error("Invalid index!")
      end

    end
  end
end
