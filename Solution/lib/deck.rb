require_relative 'card'

class Deck

  attr_reader :cards

  def self.get_cards
    cards = []
    Card.suits.map do |suit|
      Card.values.each do |value|
        cards << Card.new(suit, value)
      end
    end
    cards
  end

  def initialize
    @cards = Deck.get_cards.shuffle!
  end

  def deal(n)
    raise 'Not enough cards!' if n > self.cards.count
    cards_to_deal = self.cards.take(n)
    self.cards = self.cards.drop(n)
    cards_to_deal
  end

  private
  
  attr_writer :cards

end
