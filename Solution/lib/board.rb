require_relative 'deck'
require 'byebug'

class Board
  CARD_VALUES = {
    :deuce => 2,
    :three => 3,
    :four  => 4,
    :five  => 5,
    :six   => 6,
    :seven => 7,
    :eight => 8,
    :nine  => 9,
    :ten   => 10,
    :jack  => 11,
    :queen => 12,
    :king  => 13,
    :ace   => 1
  }

  attr_accessor :free_cells, :foundation_piles, :tableau, :deck
  def self.create_tableu_piles(deck)
    piles = Array.new(8) { Array.new }
    52.times do |n|
      idx = n % 8
      piles[idx] << deck.deal(1)[0]
    end
    piles
  end

  def initialize(deck = Deck.new)
    @deck = deck
    @free_cells = Array.new(4) { Array.new }
    @foundation_piles =  Array.new(4) { Array.new }
    @tableau = Board.create_tableu_piles(@deck)
  end

  def valid_free_cell_move?(cell_idx)
    raise "Invalid index!" if invalid_index?(cell_idx, 3)
    self.free_cells[cell_idx].empty?
  end

  def valid_foundation_pile_move?(card, pile_idx)
    raise "Invalid index!" if invalid_index?(pile_idx, 3)
    pile = self.foundation_piles[pile_idx]
    if pile.empty?
      return true if card.value == :ace
    elsif pile.last.suit != card.suit
      return false
    elsif CARD_VALUES[pile.last.value] == CARD_VALUES[card.value] - 1
      return true
    end
    false
  end

  def valid_tableau_move?(card, pile_idx)
    raise "Invalid index!" if invalid_index?(pile_idx, 7)
    pile = self.tableau[pile_idx]
    if pile.empty?
      return true
    elsif suit_color(pile.last.suit) != suit_color(card.suit)
      return true if CARD_VALUES[pile.last.value] == CARD_VALUES[card.value] + 1
    end
    false
  end

  private

  def suit_color(suit)
    return :red if [:hearts, :diamonds].include?(suit)
    :black
  end

  def invalid_index?(idx, max_idx)
    idx < 0 || idx > max_idx
  end

end
