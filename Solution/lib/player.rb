class Player

  def initialize(name)
    @name = name
  end

  def move_from
    puts "Please select a pile (:tableau or :free_cells) and index to select a card from ex :tableau, 5"
    gets.chomp.split(", ")
  end

  def move_to
    puts "Please select a destination (:tableau , :free_cells or :foundation_pile) and index to move to ex :free_cells, 2"
    gets.chomp.split(", ")
  end

end
