require_relative 'board'
require_relative 'player'

class Game

  attr_accessor :board
  attr_reader :player
  def initialize
    @board = Board.new
    @player = Player.new("Neil")
  end

  def selected_card(from_pile, from_idx)
    case from_pile
    when :tableau
      pile = board.tableau
      raise 'No card here!' if invalid_index?(pile, from_idx)
      selected_card = pile[from_idx].pop
    when :free_cells
      pile = board.free_cells
      raise 'No card here!' if invalid_index?(pile, from_idx)
      selected_card = pile[from_idx].pop
    else
      raise "Invalid pile!"
    end
    selected_card
  end

  def move_to_destination(card, to_pile, to_idx)
    case to_pile
    when :tableau
      raise "Cant move there" unless board.valid_tableau_move?(card, to_idx)
      self.board.tableau[to_idx] << card
    when :free_cells
      raise "Cant move there" unless board.valid_free_cell_move?(to_idx)
      self.board.free_cells[to_idx] << card
    when :foundation_pile
      raise "Cant move there" unless board.valid_foundation_pile_move?(card, to_idx)
      self.board.foundation_piles[to_idx] << card
    else
      raise "Invalid pile!"
    end
  end

  def play_turn(player)
    from_pile, from_idx = player.move_from
    card_to_move = selected_card(from_pile, from_idx)
    to_pile, to_idx = player.move_to
    move_to_destination(card_to_move, to_pile, to_idx)
  end

  def won?
    self.board.foundation_piles.flatten.length == 52
  end

  private

  def invalid_index?(pile, idx)
    return true if pile[idx].nil? || pile[idx].empty? || idx < 0
    false
  end
end
